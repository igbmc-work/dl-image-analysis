# Deep Learning on Image Analysis: _How DL has revolutionized the field with a couple of examples_

## Presentation on deep learning and a few applications at IGBMC

Brief presentation to introduce a few basic concepts on deep learning and bioimage analysis. I presented a few examples where DL has revolutionised the type of analysis that can be done.

## Author

Marco Dalla Vecchia (dallavem@igbmc.fr)

## Slides availability

Slides are available [here](https://igbmc.gitlab.io/mic-photon/dl-image-analysis/)
